#!/bin/bash

echo "This is the list of the processes you are currently executing: `ps ux | awk '{print $11}' | sort | uniq`"
