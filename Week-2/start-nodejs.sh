#!/bin/bash

# Install the tools

apt update

echo "Install nodejs, npm and curl"
apt install -y nodejs npm curl

echo ----Installing-----


# Display npm and node versions

NPM_VER=$(npm --version)
echo "NPM version $NPM_VER is installed"


NODE_VER=$(node --version)
echo "NodeJS version $NODE_VER is installed"

# Get the project from s3 and untar it

curl https://node-envvars-artifact.s3.eu-west-2.amazonaws.com/bootcamp-node-envvars-project-1.0.0.tgz

tar zxvf ./bootcamp-node-envvars-project-1.0.0.tgz

# Set env vars

export APP_ENV=dev
export DB_USER=myuser
export DB_PWD=mysecret

cd package

# Set log directory

echo -n "Set a log directory location for the app: "
read LOG_DIR
if [ -d $LOG_DIR ]
then
  echo "$LOG_DIR already exists"
else
  mkdir -p $LOG_DIR
  echo "New log directory has been created at $LOG_DIR "
fi


# Run the NodeJS app

npm install

node server.js &

# Confirm that node is running and listening to the port 3000

ps aux | grep node | grep -v grep


if [ `ss -lt -H -4 '( sport = 3000 )' | wc -l` != '1' ] ; then
  echo "Node is not running"
  exit 1
fi
