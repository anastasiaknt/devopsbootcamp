#!/bin/bash

echo -n "Would you like to sort the processes by memory or CPU? mem/cpu: "
read SORTBY
echo -n "How many lines you want displayed?: "
read LINES

if [ "$SORTBY" = "mem" ]
then
    ps ux --sort -rss | sort | uniq | head -n "$LINES"
elif [ "$SORTBY" = "cpu" ]
then
    ps ux --sort -%cpu | sort | uniq | head -n "$LINES"
else
    echo "No input provided. Please provide an input"
fi
