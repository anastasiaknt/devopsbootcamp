#!/bin/bash

apt update

apt install -y default-jre

JAVA_VER=$(java -version 2>&1 >/dev/null | grep "java version\|openjdk version" | awk '{print substr($3,2,2)}')

if [ "$JAVA_VER" -ge 11 ]
then
    echo You installed Java version 11 or greater
else
    echo Installation of Java failed.
fi
