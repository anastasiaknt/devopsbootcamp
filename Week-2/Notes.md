## Exercise 2

- How to install Java in Linux explained:

https://www.theserverside.com/blog/Coffee-Talk-Java-News-Stories-and-Opinions/How-do-I-install-Java-on-Ubuntu

- When tested in local the version is:
` openjdk version "11.0.16" 2022-07-19 `

- ` "java version\|openjdk version" ` - the first part should be included because despite the fact that in my computer I use openjdk and this would be enough, we are writing a script so should contemplate the other option.

- The last line of the for loop says that installation has failed when it dosen't find the desired version on the computer. (Meaning the beginning of the script went bad for some reason)


## Exercises 3-5

No additional problems found. Just kept completing the script and it executes well.

## Exercises 6-8

Artifact downloaded from:  https://node-envvars-artifact.s3.eu-west-2.amazonaws.com/bootcamp-node-envvars-project-1.0.0.tgz

- Added the `grep -v grep` after seeing the solution because did not thought about it

- Important -p flag when creating the log directory.

- Important to note the & in order for the node app to run in background.

- Created a if look to check if node is running. Realized that in solutions net-tools were used to confirm that but I prefer to use ss because its always present in every Linux machine.
